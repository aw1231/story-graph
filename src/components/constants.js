module.exports = {
	move_in: 'MOVE_IN',
	move_out: 'MOVE_OUT',
	stay: 'STAY',
	appear: 'APPEAR',
	vanish: 'VANISH',
	encounter: 'ENCOUNTER',
	target: 'TARGET',
	source: 'SOURCE'
}