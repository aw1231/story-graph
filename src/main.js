module.exports = {
	constants: require('./components/constants.js'),
	Thing: require('./components/thing.js'),
	Rule: require('./components/rule.js'),
	Type: require('./components/type.js'),
	Location: require('./components/location.js'),
	World: require('./world/world.js')
}